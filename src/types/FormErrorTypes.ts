export type FormErrors = {
	firstName: string;
	lastName: string;
	email: string;
};
