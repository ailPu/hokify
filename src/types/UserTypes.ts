export type UserObject = {
	firstName: string;
	lastName: string;
	email: string;
};
