export type StyleObject = { [key: string]: string | boolean };
